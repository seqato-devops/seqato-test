package com.seqato.seqatotest

/**
 * @author Samuel Robert <samuel.robert@seqato.com>
 * @created on 06 Sep 2018 at 1:00 PM
 *
 * TODO: Use this class for managing API and other storage interactions
 * TODO: For API using Retrofit will be a plus
 * TODO: Using Dagger for dependency Injection will be a plus
 */
class AuthRepository