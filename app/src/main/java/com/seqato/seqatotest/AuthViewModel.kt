package com.seqato.seqatotest

import android.arch.lifecycle.ViewModel

/**
 * @author Samuel Robert <samuel.robert@seqato.com>
 * @created on 06 Sep 2018 at 1:00 PM
 *
 * TODO: Use this class for storing view related data
 * TODO: You may use LiveData to handle the interaction between views and repository
 * TODO: Using Dagger for dependency Injection will be a plus
 */
class AuthViewModel : ViewModel()