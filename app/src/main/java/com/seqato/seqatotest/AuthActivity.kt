package com.seqato.seqatotest

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * @author Samuel Robert <samuel.robert@seqato.com>
 * @created on 06 Sep 2018 at 1:00 PM
 *
 * @see AuthViewModel
 * @see AuthRepository
 *
 * TODO: Writing test cases will be a plus
 */
class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // TODO: Use vm to save UI data into the view model
        val vm: AuthViewModel = ViewModelProviders.of(this)[AuthViewModel::class.java]
    }
}
